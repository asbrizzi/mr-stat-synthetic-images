# MR-STAT synthetic images

Repository with example images from a prospective cross-sectional clinical trial with Magnetic Resonance Spin TomogrAphy in Time-domain (MR-STAT).  
Images are in compressed NIfTI (nii.gz) format and contain 30 slices with a spatial resolution of 1 x 1 x 3 mm and a 1.5 mm gap.  
An example participant is selected from each included group: healthy, primary brain tumor, epilepsy, multiple sclerosis (MS) and ischemic stroke.  
The conventional (Conv) and synthetically (Synt) generated MR-STAT T1w, T2w, PDw and FLAIR are presented. 
In addition, the quantitative (q) T1, T2 and PD map from which the synthetic contrast weighted images are generated are presented.

## Authors and acknowledgment

This repository belongs to the results presented in:  
_Synthetic MRI with Magnetic Resonance Spin TomogrAphy in Time-domain (MR-STAT): results from a prospective cross-sectional clinical trial_  
by Jordi P.D. Kleinloog PhD, Stefano Mandija PhD, Federico D'Agata PhD, Hongyan Liu, Oscar van der Heide, Beyza Koktas, Sarah M. Jacobs MD, Cornelis A.T. van den Berg PhD, Jeroen Hendrikse MD PhD, Anja G. van der Kolk MD PhD, and Alessandro Sbrizzi PhD

